# 테스트 설명
테스트는 제출되는 산출물(결과) 평가와 동시에 문제를 해결하시는 과정을 평가합니다.   
해당 Repository 를 Clone 받으셨다면, 과제 수행을 위해 **result** 브랜치를 생성하신 후 아래 과제 내용을 
수행하여 리모트(remote) 로 push해 주시면 됩니다.(bitbucket 계정이 없는 경우 skip)  
개발 환경은 [Vue.js](https://vuejs.org/)를 기반으로 하는 SPA 환경으로 세팅하여(Vue Cli 활용 가능) 개발하시고  
개발이 완료되시면 참석한 시니어 개발자와 함께 간단한 코드 리뷰(개발코드 어필)를 진행한 후 테스트가 종료됩니다.  
짧은 시간에 진행되는 테스트로 큰 기능을 중심으로 개발하시고, 시간적 여유가 되신다면 상세기능을 개발/설정 하시는 것을 추천 드립니다.   


**Google meet 접속하시면 오른쪽 아래에 [발표시작 > 내 전체 화면] 클릭하여 화면을 공유 부탁드립니다.**



---
# 테스트 미션 
### 주제: 상품 리스트( 예시: [비스토어 상품리스트](https://www.bstore.site/market))
#### - 제공된 [REST API](https://dev.clayful.io/ko/http/apis/product/list)를 활용하여 상품 리스트를 조회(/v1/products)하고 **30개**씩 페이징하여 화면에 노출합니다.

### 상세조건: 

##### 1. 상품 리스트 페이지 구성시 노출될 필수 속성은 아래와 같습니다.
```
필수:
    - 상품명(name)
    - 상품가격(price.original)
    - 브랜드(brand)
    - 페이징 정보(상품 총 수, 현재 페이지 번호)
옵션:
    - 썸네일(thumbnail)
```
##### 2. 개발된 상품 리스트 페이지는 **/event/products** 에서 라우팅 될 수 있도록 설정합니다.
##### 3. 상품 리스트 페이지 구성시 **1개 이상의 vue compoment를 생성하고 활용**합니다. (제안: 단일상품)
##### 4. 아래 제공되는 API 접근 토큰은 별도의 설정파일(.env)에 설정하고 활용합니다. 
```
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Ijg3NDNlYjJmOWY4OGRhNzRlNmI4ZDJhMzZhZDczZDNlMDVmNjc5YTMzZTVjNWY5ZTQ0YjkxNDcwOTE0ZThkNTciLCJyb2xlIjoiY2xpZW50IiwiaWF0IjoxNjA0Mjg5ODc4LCJzdG9yZSI6IkhWWjlFTlI1OUhaSi42R1hGOVJaUTdTR0IiLCJzdWIiOiI3ODZLSDU5NkxDUUUifQ.J0MuyLKjyrWRf0P9rGwKex0ydmIJfS_jPV2o-gdsoeA
``` 



### 기타조건:
##### 1. 총 테스트 시간: 60분(개발: 50분, 리뷰: 10분) - 추가 시간이 필요하신 경우 최대 90분까지 사용 가능합니다.
##### 2. **질문 및 추가적으로 필요한 정보가 있을 경우 시니어 개발자에게 언제든지 질문/요청 할 수 있습니다.**
##### 3. 문제 해결과 관련하여 검색을 자유롭게 활용할 수 있습니다.
##### 4. 화면은 자유롭게 구성할 수 있으며, [vuetifyjs](https://vuetifyjs.com/en/)와 같은 UI framework를 활용할 수 있습니다.



---
# 가산 포인트 
#### - ES6 / Typescript 활용
#### - Vuex 활용
#### - Axios 모듈화


---
# Tip 
#### 시니어 개발자를 적극 활용하세요! 시니어 개발자는 결과보다 과정을 중시합니다.:sunglasses: